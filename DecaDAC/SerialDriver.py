#!/usr/bin/env python

import serial
import InstrumentDriver
from InstrumentConfig import InstrumentQuantity
import numpy as np
# for long integer py2/py3 compatibility
#from builtins import int

WARNING, INFO, DEBUG = 30, 20, 10
TIMEOUT = 0.05 

COMM_ERROR_MESSAGE = "Instrument reply does not match the specifications! ("
class Error(Exception):
    pass

class SerialDriver(InstrumentDriver.InstrumentWorker):
    """ This class implements the DecaDAC driver"""

    def performOpen(self, options={}):
        """Perform the operation of opening the instrument connection"""
        addr = self.getAddress()
        comCfg = self.getCommunicationCfg()
        try:
            self.serialCom = serial.Serial(port=addr,
                                           baudrate=comCfg['Baud rate'],
                                           parity=serial.PARITY_NONE,
                                           bytesize=serial.EIGHTBITS)
        
        except Exception as e:
            raise InstrumentDriver.CommunicationError(str(e))
            
    def performClose(self, bError=False, options={}):
        """Perform the close instrument connection operation"""
        self.serialCom.close()

    def performSetValue(self, quant, value, sweepRate=0.0, options={}):
        """Perform the Set Value instrument operation"""
        cmd_str = quant.set_cmd.replace('<*>', str(int(value)))
        self.writeAndLog(cmd_str)
        return self.performGetValue(quant)
   
    def performGetValue(self, quant, options={}):
        """Perform the Get Value instrument operation"""
        reply = self.askAndLog(quant.get_cmd)
        return reply.split(b"!")[-2][1:]
    
    def performStopSweep(self, quant, options={}):
        self.write(quant.stop_cmd)        
        return self.performGetValue(quant)
    
    def ask(self, sCmd, bCheckError=True):
        self.serialCom.write(sCmd.encode('utf-8'))
        self.wait(TIMEOUT)
        reply = self.serialCom.read_all()
        if bCheckError and reply.count(b"!") != sCmd.count(";"):
            raise InstrumentDriver.CommunicationError(COMM_ERROR_MESSAGE
                                                      + reply.decode('utf-8')
                                                      + ")")
        return reply
    
    def askAndLog(self, sCmd, bCheckError=True):
        reply = self.ask(sCmd, bCheckError)
        self.log("INSTRUMENT REPLY: '{}'".format(reply), level=DEBUG)
        return reply
    
    def write(self, sCmd, bCheckError=True):
        self.serialCom.write(sCmd.encode('utf-8'))                                                                          
        self.wait(TIMEOUT)
        reply = self.serialCom.read_all()
        if bCheckError and reply.count(b"!") != sCmd.count(";"):
            raise InstrumentDriver.CommunicationError(COMM_ERROR_MESSAGE
                                                      + reply.decode('utf-8')
                                                      + ")")
        return
    
    def writeAndLog(self, sCmd, bCheckError=True):
        self.write(sCmd, bCheckError)
        self.log("INSTRUMENT WRITE: '{}'".format(sCmd), level=DEBUG)
        return 
if __name__ == '__main__':
    pass

