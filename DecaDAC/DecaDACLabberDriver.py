#!/usr/bin/env python
import SerialDriver
from InstrumentConfig import InstrumentQuantity
import numpy as np
import time

class Error(Exception):
    pass

class Driver(SerialDriver.SerialDriver):
    """ This class implements the DecaDAC driver"""

    def performSetValue(self, quant, value, sweepRate=0.0, options={}):
        """Perform the Set Value instrument operation"""

        self.log('sweep_rate')
        self.log(sweepRate)

        #For storing multiple sweep commands an simultaneous execution:
        if self.isFirstCall(options):
            self.log('FirstCall' + str(quant.name))
            self.collect_sweep_cmds = {}
            self.collect_sweep_params = {}
            self.max_sweep_time = 0
            self.sweep_end_time = 0

        #Range settings are virtual
        if "range" in quant.section.lower():
            if self.__captchaGenerator__("Switching ranges has to be done "
                                      "via\nthe instruments hardware switches!"):
                return value
            else:
                return self.performGetValue(quant)
        
        #Safety measures for output settings
        if "M<*>;" in quant.set_cmd:
            if quant.datatype == 1:
                value = 2 if value else 0
            # Check if channels are all zero
            slot = quant.name.split(" - ")[0]
            channel_suffixes = ["Channel 0", "Channel 1",
                                "Channel 2", "Channel 3"]
            slot_channels = ["{} - {}".format(slot, chan) for chan in channel_suffixes]
            # Otherwise display a warning
            if not self.performSafetyCheck(slot_channels, 0):

                if self.__captchaGenerator__("One or several channels have" 
                                             " a non-zero\n"
                                             "voltage, this will lead"
                                             " to steep voltage edges!"):
                    pass
                else:
                    return self.performGetValue(quant)
                
        #Convert float to 16bit for setting voltages
        if "D<*>;" in quant.set_cmd:
            binValue = self.__convertToBin__(value, quant.name)
        else:
            binValue = value

        #Sweeping
        if sweepRate == 0.0:
            return super(Driver, self).performSetValue(quant,
                        binValue, sweepRate, options)
        else:
            current_value = self.performGetValue(quant)
            voltage_diff = value - current_value
            self.log('voltage_diff' + str(voltage_diff))


            
            bin_diff = self.__convertToBin__(value, quant.name) - self.__convertToBin__(current_value, quant.name)
            #self.log('bin_diff ' + str(bin_diff))

            if bin_diff == 0 :
                if self.isFinalCall(options) and len(self.collect_sweep_cmds) > 0:
                    self.log('FinalCall and sending' + str(quant.name))
                    self.send_sweep_cmds()
                return value
            
            timesteps = abs(voltage_diff/sweepRate*1000)
            if timesteps < 1:
                raise InstrumentDriver.Error('SweepRate is too high!')
                
            upper_limit = value if voltage_diff > 0 else current_value
            lower_limit = current_value if voltage_diff>0 else value
            upper_limit = str(self.__convertToBin__(upper_limit, quant.name))
            lower_limit = str(self.__convertToBin__(lower_limit, quant.name))             


            slope = str(int(round(bin_diff/timesteps*65536)))
            

            cmd_str = quant.sweep_cmd.replace('<ll>',
                                              lower_limit).replace('<ul>',
                                                         upper_limit).replace('<sr>',slope)
            self.log(cmd_str)
                                              
            self.collect_sweep_cmds[quant.name] = cmd_str
            self.collect_sweep_params[quant.name] = [bin_diff, int(slope)]

        if self.isFinalCall(options):
            self.log('FinalCall')

        if self.isFinalCall(options) and len(self.collect_sweep_cmds) > 0 :
            self.log('FinalCall and sending' + str(quant.name))
            self.send_sweep_cmds()
        
        return self.__convertToVoltage__(self.__convertToBin__(value, quant.name), quant.name)
        
    
    def performSafetyCheck(self, quantities, target_value=0):
        for quant in quantities:
            if not self.readValueFromOther(quant) == target_value:
                return False
        return True

    def checkIfSweeping(self, quant, options={}):
        self.log('CheckIfSweeping ' + quant.name)

        #Check if enough time passed for sweep(s) to have ended
        if time.time() > self.sweep_end_time :
            still_sweeping = int(self.askAndLog(quant.sweep_check_cmd).split(b"!")[-2][1:])
            
            if still_sweeping:
                return still_sweeping
            else:
                self.performStopSweep(quant, options={} ) #performStopSweep is executed in order to get the lower and upper Voltage limits back to default
                return False
        else:
            self.log('Not enough time passed, still sweeping')
            return True


    def performGetValue(self, quant, options={}):
        """Perform the Get Value instrument operation"""

        #Pure virtual range quantity
        if "range" in quant.section.lower():
            return quant.getValue()

        response = super(Driver, self).performGetValue(quant, options)
        
        if "m;" in quant.get_cmd:
            return bool(int(response))

        if "d;" in quant.get_cmd:
            self.log('Voltage before readout, {}'.format(quant.name))
            self.log(quant.getValue())
            return self.__convertToVoltage__(response, quant.name)
        else:
            return response
    
    def __captchaGenerator__(self, message):
        rd1, rd2 = np.random.randint(0,10,size=(2))
        text = message + "\n"
        text += "To continue anyway, calculate {} + {} = ".format(rd1, rd2)
        uinput = self.getValueFromUserDialog(value=0,
                                             text=text,
                                             title="OPERATION DANGEROUS!")
        return uinput == rd1 + rd2
    
    def __convertToBin__(self, voltage, channel):
        lower, delta = self.__getChannelRanges__(channel)
        return int(max(0,min((round((voltage-lower)/delta * 65535), 65535))))
    
    def __convertToVoltage__(self, binary, channel):
        lower, delta = self.__getChannelRanges__(channel)
        return (float(binary)*delta/65535.) + lower

    
    def __getChannelRanges__(self, channel):
        channelRangeCombo = self.getValue("{}.{} - Range switch".format(channel[0], channel[-1]))
        
        if channelRangeCombo == "-10V...+10V":
            return -10, 20
        elif channelRangeCombo == "-10V...0V":
            return -10, 10
        elif channelRangeCombo == "0V...+10V":
            return 0, 10
        else:
            raise InstrumentDriver.CommunicationError("INVALID RANGE SETTINGS!")
        

    def send_sweep_cmds(self):
        for quant_name, sweep_cmd_str in self.collect_sweep_cmds.items():
            self.log('Sent sweep command for {}'.format(quant_name))
            self.writeAndLog(sweep_cmd_str)

            #calculate Duration of sweep (Slope value is 32bit, slope is added to current value every ms (thus *65536 / 1000))
            binDiff = self.collect_sweep_params[quant_name][0] 
            sweep_slope = self.collect_sweep_params[quant_name][1]
            

            
            sweep_time = abs(binDiff / sweep_slope * 65536 / 1000) #sweep time in seconds


            if sweep_time > self.max_sweep_time:
                self.max_sweep_time = sweep_time

        self.sweep_end_time = self.max_sweep_time + time.time()





    def performStopSweep(self, quant, options={}):
        self.log('Sending Stop Command for Sweep of {}'.format(quant.name))

        #Correcting the upper and lower limits of the channel in order to allow for the full range again
        corret_limits_and_stop = quant.sweep_cmd.replace('<ll>',str(0)).replace('<ul>',str(65535)).replace('<sr>',str(0))
        self.write(corret_limits_and_stop)   
        return self.performGetValue(quant)

if __name__ == '__main__':
    pass

