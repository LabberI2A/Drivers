# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 18:36:00 2018

@author: tebbe
"""

import InstrumentDriver
import numpy as np

class Driver(InstrumentDriver.InstrumentWorker):
    """ This class implements a simple signal generator driver"""
    

    def performOpen(self, options = {}):
        """Perform the operation of opening the instrument connection"""
        pass


    def performClose(self, bError = False, options = {}):
        """Perform the close instrument connection operation"""
        pass


    def performSetValue(self, quant, value, sweepRate = 0.0, options = {}):
        """Perform the Set Value instrument operation. This function should
        return the actual value set by the instrument"""
        # just return the value
        return value


    def performGetValue(self, quant, options = {}):
        """Perform the Get Value instrument operation"""
        # proceed depending on quantity
        if quant.name  ==  'Signal':

            # if asking for signal, start with getting values of other controls
            DATAPATH = self.getValue('Datapath')
            skiph = int(self.getValue('Skip Header'))
            skipf = int(self.getValue('Skip Footer'))
            delim = self.getValue('Delimiter')
            if not len(delim):
                delim = ' '
            scale = float(self.getValue('Scale Factor'))
            max_signal = float(self.getValue('Max Values'))
            min_signal = float(self.getValue('Min Values'))
            self.log(max_signal)
            self.log(min_signal)

            data =  np.genfromtxt(DATAPATH, delimiter  =  delim, skip_header = skiph, skip_footer = skipf)
            time = data[:,0]
            signal = data[:,1] * scale
            add_noise  =  self.getValue('Add noise')
            if add_noise:
                noise_amp  =  self.getValue('Noise amplitude') * scale
                signal +=  noise_amp * np.random.randn(len(signal))

            if int(self.getValue('CutWaveform')):
                for i,j in enumerate(signal):
                    self.log(signal[i])
                    if signal[i] > max_signal:
                        signal[i] = max_signal
                    if signal[i] < min_signal:
                        signal[i] = min_signal

            # create trace object that contains timing info
            self.log(signal, level = 30)
            self.log(time, level = 30)
            trace  =  quant.getTraceDict(signal,time)
            # finally, return the trace object
            return trace
        else: 
            # for other quantities, just return current value of control
            return quant.getValue()
           


