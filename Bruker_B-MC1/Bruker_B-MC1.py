#!/usr/bin/env python
#editor Timo B.
#self.wait(0.1) ## added to reduce fails due to communication losses
import InstrumentDriver
from VISA_Driver import VISA_Driver
import visa
from InstrumentConfig import InstrumentQuantity

__version__ = "0.0.1"

class Error(Exception):
	pass

class Driver(VISA_Driver):
	""" This class implements the Lakeshore 475 driver"""
	
	def performOpen(self, options={}):
		"""Perform the operation of opening the instrument connection"""
		VISA_Driver.performOpen(self, options=options)

	def performClose(self, bError=False, options={}):
		"""Perform the operation of opening the instrument connection"""
		self.writeAndLog("PTC= 0.")
		VISA_Driver.performClose(self, bError, options=options)

	
	def performGetValue(self, quant, options={}):
		"""Perform the Get Value instrument operation"""
		try:
			if (quant.name in ('Output unit','Absolute deviation','Relative deviation','offset in B-Field calculation')):
				return quant.getValue()
			value=self.askAndLog(quant.get_cmd)
			l=value.split(' ')
			if len(l)<2:
				return 0
			value=float(l[-1])
			if l[0] == '-':
				value= (-1)*value
			if (quant.name in ('B-Field')):
				offset=self.getValue('offset in B-Field calculation') #7mT war bei test und ist jetzt standard
				params=[-2.14236783e-04,-1.32015291e-03,1.80024778e+01,-5.85129355e+00]
				magfield=params[3]+params[2]*value+params[1]*value**2+params[0]*value**3
				magfield=round(magfield,3)
				magfield-=offset
				return magfield
			return value
		except Error as e:
			return quant.getValue()


	#0.1A + 1% von set wert
	def performSetValue(self, quant, value, sweepRate=0.0, options={}):
		"""Perform the set value operation"""
		# check type of quantity
		try:
			if (quant.name in ('B-Field','Current')):
				deviation=[self.getValue('Absolute deviation'),self.getValue('Relative deviation')/100] #0.1A + 1% von set wert ist standart
				value=round(float(value),3)
				if (quant.name in ('B-Field')):
					offset=self.getValue('offset in B-Field calculation') #7mT war bei test und ist jetzt standart
					value+=offset
					params=[1.87879038e-09,2.49701790e-07,5.55849318e-02,3.27814017e-01]
					current=params[3]+params[2]*value+params[1]*value**2+params[0]*value**3
					current=round(current,3)
					self.writeAndLog(quant.set_cmd.replace('<*>', str(current)))
					self.wait(0.1) ## added to reduce fails due to communication losses
					self.waitForReachingValue(quant,current,deviation)# here often an error occurs. Chekc the function!
					value-=offset
				else: 
					self.writeAndLog(quant.set_cmd.replace('<*>', str(value)))
					self.wait(0.1) ## added to reduce fails due to communication losses
					self.waitForReachingValue(quant,value,deviation)
			elif (quant.name in ('max Current Rate')):
				self.writeAndLog(quant.set_cmd.replace('<*>', str(round(value,1))))
			return value
		except Error as e:
			return value
			
	def waitForReachingValue(self, quant,value, deviation):
		present=self.askAndLog(quant.get_cmd)
		l=present.split(' ')
		if len(l)<2: 
			self.writeAndLog(quant.set_cmd.replace('<*>', str(0)))
			l[-1]='0'
		present=float(l[-1])
		avalue=abs(value)
		while not ((avalue-deviation[0]-deviation[1]*avalue) < present < (avalue+deviation[0]+deviation[1]*avalue)) and not self.isStopped() :
			self.wait(1) #wait till new request of current value, set from 0.3 to 1, since the power supply cannot answer so fast
			present=self.askAndLog(quant.get_cmd)
			l=present.split(' ')
			present=float(l[-1])
		if self.isStopped():
			self.writeAndLog(quant.set_cmd.replace('<*>', str(0)))
		
if __name__ == '__main__':
	pass