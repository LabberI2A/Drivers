# Instrument driver configuration file.

[General settings]

# The name is shown in all the configuration windows
name: AWG520

# The version string should be updated whenever changes are made to this config file
version: 1.0

# Name of folder containing the code defining a custom driver. Do not define this item
# or leave it blank for any standard driver based on the built-in VISA interface.
driver_path: 



[Model and options]
# The option section allow instruments with different options to use the same driver

# Check instrument model id at startup (True or False). Default is False
check_model: False



# General VISA settings for the instrument.
[VISA settings]

# Enable or disable communication over the VISA protocol (True or False)
# If False, the driver will not perform any operations (unless there is a custom driver).
use_visa = True

# Reset the interface (not the instrument) at startup (True or False).  Default is False
reset: False

# Time (in seconds) before the timing out while waiting for an instrument response. Default is 5
timeout: 5

# Query instrument errors (True or False).  If True, every command sent to the device will
# be followed by an error query.  This is useful when testing new setups, but may degrade
# performance by slowing down the instrument communication. 
query_instr_errors: False 

# Bit mask for checking status byte errors (default is 255, include all errors)
# The bits signal the following errors:
# 0: Operation
# 1: Request control
# 2: Query error
# 3: Device error
# 4: Execution error
# 5: Command error
# 6: User request
# 7: Power on
error_bit_mask: 255

# SCPI string to be used when querying for instrument error messages
error_cmd: :SYST:ERR?

# Initialization commands are sent to the instrument when starting the driver
# *RST will reset the device, *CLS clears the interface
init: 

# Final commands sent to the instrument when closing the driver
final: 


# Define quantities in sections. This list is a selection of allowed keywords,
# see the manual for a full list of options
#   datatype:      The datatype should be one of DOUBLE, BOOLEAN, COMBO,
#                  STRING, COMPLEX, VECTOR, VECTOR_COMPLEX, PATH or BUTTON.
#   unit:          Quantity unit
#   set_cmd:       Command used to send data to the instrument. Put <*> where the value should appear.
#   get_cmd:       Command used to get the data from the instrument. Default is set_cmd?
#   def_value:     Default value
#   low_lim:       Lowest allowable value.  Defaults to -INF
#   high_lim:      Highest allowable values.  Defaults to +INF
#   combo_def_1:   First option in a pull-down combo box. Only used when datatype=COMBO
#   combo_def_2:   Second option in a pull-down combo box. Only used when datatype=COMBO
#   ...
#   combo_def_n:   nth option in a pull-down combo box. Only used when datatype=COMBO
#   state_quant:   Quantity that determines this control's visibility
#   state_value_1: Value of "state_quant" for which the control is visible
#   state_value_2: Value of "state_quant" for which the control is visible
#   ...
#   state_value_n: Value of "state_quant" for which the control is visible
#   permission:    Sets read/writability, options are BOTH, READ, WRITE or NONE. Default is BOTH 
#   group:         Name of the group where the control belongs.
#   section:       Name of the section where the control belongs.


[Run]
datatype: BUTTON
tooltip: Set instrument to run mode
set_cmd: AWGC:RUN
show_in_measurement_dlg: True

[Stop]
datatype: BUTTON
tooltip: Stop waveform output
set_cmd: AWGC:STOP
show_in_measurement_dlg: True

[Sampling rate]
datatype: DOUBLE
unit: Hz
def_value: 1E9
low_lim: 50E3
high_lim: 1E9
set_cmd: :FREQ
show_in_measurement_dlg: True

[Reference]
datatype: COMBO
def_value: Internal
combo_def_1: Internal
combo_def_2: External
cmd_def_1: INT
cmd_def_2: EXT
set_cmd: :ROSC:SOUR

[Ch1 On/Off]
datatype: BOOLEAN
def_value: 0
set_cmd: OUTP1:STAT

[Ch2 On/Off]
datatype: BOOLEAN
def_value: 0
set_cmd: OUTP2:STAT

[FG On/Off]
datatype: COMBO
def_value: OFF
combo_def_1: ON
combo_def_2: OFF
cmd_def_1: 1
cmd_def_2: 0
set_cmd: :AWGC:FG

[FG - Frequency]
datatype: DOUBLE
unit: Hz
low_lim: 1
high_lim: 100E6
set_cmd: :AWGC:FG:FREQ
show_in_measurement_dlg: True


[Ch1 - FG Waveform]
label: Funktionsgenerator
datatype: COMBO
combo_def_1: Sinus
combo_def_2: Triangle
combo_def_3: Square
combo_def_4: Ramp
combo_def_5: Pulse
combo_def_6: DC
cmd_def_1: SIN
cmd_def_2: TRI
cmd_def_3: SQU
cmd_def_4: RAMP
cmd_def_5: PULS
cmd_def_6: DC
set_cmd: :AWGC:FG1:FUNC 
group: Channel 1
section: Channels 

[Ch1 - FG Phase]
label: FG Phase
datatype: DOUBLE
low_lim: -6.28319
high_lim: 6.28319
set_cmd: :AWGC:FG1:PHAS 
group: Channel 1
section: Channels 

[Ch1 - FG Polarity]
label: Funktionsgenerator
datatype: COMBO
combo_def_1: Positiv
combo_def_2: Negativ
cmd_def_1: POS
cmd_def_2: NEG
set_cmd: :AWGC:FG1:POL
group: Channel 1
section: Channels 

[Ch1 - FG Duty Cycle]
label: FG Duty Cycle
datatype: DOUBLE
low_lim: 0.1
high_lim: 99.9
set_cmd: :AWGC:FG1:PULS:DCYC
group: Channel 1
section: Channels 

[Ch1 - FG Vpp]
label: FG Peap to Peak Amplitude
datatype: DOUBLE
low_lim: 0.02
high_lim: 2
set_cmd: :AWGC:FG1:VOLT
unit: V
group: Channel 1
section: Channels 

[Ch1 - FG Offset]
label: FG Offset
datatype: DOUBLE
low_lim: -1
high_lim: 1
set_cmd: :AWGC:FG1:VOLT:OFFS
unit: V
group: Channel 1
section: Channels 


[Ch2 - FG Waveform]
label: Funktionsgenerator
datatype: COMBO
combo_def_1: Sinus
combo_def_2: Triangle
combo_def_3: Square
combo_def_4: Ramp
combo_def_5: Pulse
combo_def_6: DC
cmd_def_1: SIN
cmd_def_2: TRI
cmd_def_3: SQU
cmd_def_4: RAMP
cmd_def_5: PULS
cmd_def_6: DC
set_cmd: :AWGC:FG2:FUNC 
group: Channel 2
section: Channels 

[Ch2 - FG Phase]
label: FG Phase
datatype: DOUBLE
low_lim: -6.28319
high_lim: 6.28319
set_cmd: :AWGC:FG2:PHAS 
group: Channel 2
section: Channels 

[Ch2 - FG Polarity]
label: Funktionsgenerator
datatype: COMBO
combo_def_1: Positiv
combo_def_2: Negativ
cmd_def_1: POS
cmd_def_2: NEG
set_cmd: :AWGC:FG2:POL
group: Channel 2
section: Channels 

[Ch2 - FG Duty Cycle]
label: FG Duty Cycle
datatype: DOUBLE
low_lim: 0.1
high_lim: 99.9
set_cmd: :AWGC:FG2:PULS:DCYC
group: Channel 2
section: Channels 

[Ch2 - FG Vpp]
label: FG Peap to Peak Amplitude
datatype: DOUBLE
low_lim: 0.02
high_lim: 2
set_cmd: :AWGC:FG2:VOLT
unit: V
group: Channel 2
section: Channels 

[Ch2 - FG Offset]
label: FG Offset
datatype: DOUBLE
low_lim: -1
high_lim: 1
set_cmd: :AWGC:FG2:VOLT:OFFS
unit: V
group: Channel 2
section: Channels 
