#!/usr/bin/env python

from VISA_Driver import VISA_Driver
import numpy as np


class Driver(VISA_Driver):
    """ This class implements the Keysight 33622A AWG"""

    def performOpen(self, options={}):
        """Perform the operation of opening the instrument connection"""
        # add compatibility with pre-python 3 version of Labber
        if not hasattr(self, 'write_raw'):
            self.write_raw = self.write
        # Call the generic VISA open to make sure we have a connection
        VISA_Driver.performOpen(self, options)
        # clear value of waveforms
        self.setValue('Arb. Waveform', [])
        self.waves = [None]

    def performSetValue(self, quant, value, sweepRate=0.0, options={}):
        """Perform the Set Value instrument operation. This function should
        return the actual value set by the instrument"""
        # keep track of if waveform is updated, to avoid sending it many times
        if self.isFirstCall(options):
            self.bWaveUpdated = False
        if quant.name in ('Arb. Waveform'):
            # set value, then mark that waveform needs an update
            quant.setValue(value)
            self.bWaveUpdated = True
        else:
            value = VISA_Driver.performSetValue(self, quant, value, sweepRate,
                                                    options=options)
            
        # if final call and wave is updated, send it to AWG
        if self.isFinalCall(options) and self.bWaveUpdated:
            # Double check that the waveform truly changed
            if not np.array_equal(self.getValueArray('Arb. Waveform'), self.waves):
                # store and send waveforms
                self.waves = np.copy(self.getValueArray('Arb. Waveform'))
                self.sendWaveform()
        #if in Pulsefunction sync period and freqency
        if self.getValue('Function') in ('Pulse') and quant.name in ('Frequency'):
            perd = 1/value
            self.sendValueToOther('Pulse period', perd)
        
        return value

    def sendWaveform(self):
        """Rescale and send waveform data to the AWG"""
        Vpp = self.getValue('Amplitude')
        self.log(Vpp)
        # get data
        data = self.getValueArray('Arb. Waveform')
        # get range and scale to U16
        datanor = self.scaleWaveform(data, Vpp)
        # write header + data
        header = ':DATA VOLATILE'
        # write header + data
        for i in datanor:
            header+=', '+str(i)
        self.writeAndLog(header)
        # select  waveform
        self.writeAndLog(':DATA:COPY LABBER, VOLATILE')
        self.writeAndLog(':FUNC:USER LABBER')
        # Setting ARB resets amplitude and offset, so we set those again
        self.sendValueToOther('Amplitude', Vpp)
        self.sendValueToOther('Offset', self.getValue('Offset'))

    def scaleWaveform(self, data, Vpp):
        """Scales the waveform and returns data in intervall from -1 to 1"""
        data = np.array(data)/np.max(data)
        return np.array(data)


if __name__ == '__main__':
    pass
