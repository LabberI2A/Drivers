General Contribution / Workflow
===============================

If changes or adaptions to an instrument driver are implemented, it would be
favourable to do this work on a [new branch](https://git.rwth-aachen.de/LabberI2A/Drivers/branches/new)!
If the work is sufficient to share, generate a merge request ([How-To](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)).

Merge Request
=============

When submitting a merge request, do the following things:

- [ ] Update your branch (when checked out) by merging the current master:
```
git checkout <my_branch>
git merge origin/master
git push
```
        
- [ ] Add a new [merge-request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)

- [ ] Make sure the description contains a short overview of changes, as well as a list devices you tested your changes with.